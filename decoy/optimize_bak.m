function optimize()

    clear all
    close all

    eta = 0.1;
    t_b = 1;
    p_d = 1e-5;
    V = 0.99;
    f_EC = 1.2;
    
    
    tdb=0:0.1:40;
    t=10.^(-tdb/10);
    %figure()    
    %plot(tdb,t)

    function out = h(x)
        out = (-x.*log(x)-(1-x).*log(1-x))./log(2);
    end;

    function out = A(mu,m)
        out = (1 - exp(-mu.*m)) ./ (1 - exp(-mu));
    end;

    function out = P(mu,m,t)
        out = A(mu,m).*(1 - exp(-mu.*eta.*t_b.*t));
    end;

    function out = Pd(mu,m,t)
        out = 2.*p_d.*(exp(-m.*mu) + A(mu,m).*(exp(-mu.*eta.*t_b.*t) - exp(-mu)) );
    end;

    function out = Rns(mu,m,t)
        out = P(mu,m,t) + Pd(mu,m,t);
    end;

    function out = R1ns(mu,m,t)
%         p = P(mu,m,t);
%         pd = Pd(mu,m,t);
%         p2 = (1-exp(-m.*mu)-A(mu,m).*mu.*exp(-mu));
        out = P(mu,m,t) + Pd(mu,m,t) - (1-exp(-m.*mu)-A(mu,m).*mu.*exp(-mu));
    end;

    function out = Q(mu,m,t)
        out = (0.5.*(1-V).*P(mu,m,t) + Pd(mu,m,t)./2) ./ Rns(mu,m,t);
    end;
    
    function out = Kns(mu,m,t)
        r1 = R1ns(mu,m,t);
        r = Rns(mu,m,t);
        q = Q(mu,m,t);
        q1 = q.*r./r1;
        ris = -(r1.*(1 - h(q1)) - f_EC.*r.*h(q));
        if ((r1<0) | (r1>1))
            out = 0;
        elseif ((q<0) | (q>0.5) | (q1<0) | (q1>0.5))
            out = 0;
        else
            out = ris;
        end;
        %if imag(ris) > 0
        %    out = 0;
        %else
        %    out = ris;
        %end;
    end;
    
    function out = s(t)
        P = eta.*t_b.*t;
 		Pd = 2.*p_d.*(1-eta.*t_b.*t);
 		Q = (0.5.*(1-V).*P + Pd./2) ./ (P + Pd);
        out = (P + Pd).*(1-(1+f_EC).*h(Q));
    end;
    
    % if method is absent or is 0, MATLAB minimization is used
    % if method is 1, the array is computed and the minimum is found
    %   by comparing the different values - VERY SLOW
    function out = multi(t,m,method)
        Karr = zeros(size(t));
        ii=1;
        for i = t
            if (nargin < 3) | (method == 0)
                mu0 = eta.*t_b.*i.*0.8;
                min = fminsearch(@(x) (Kns(x,m,i)+1),mu0);
                Karr(ii) = -Kns(min,m,i);
            else
                mu0 = eta.*t_b.*i;
                mu0arr = 0:0.000001:(mu0+0.0001);
                [minf,mini] = min(Kns(mu0arr,m,i));
                Karr(ii) = -minf;
            end;
            ii=ii+1;
        end;
        out = Karr;
    end;
    
    function out = compare(t,m)
        Karr1 = zeros(size(t));
        Karr2 = zeros(size(t));
        ii=1;
        for i = t
            mu0 = eta.*t_b.*i;
            min1 = fminsearch(@(x) Kns(x,m,i),mu0);
            Karr1(ii) = -Kns(min1,m,i);
            mu0arr = 0:0.000001:(mu0+0.0001);
            [minf,mini] = min(Kns(mu0arr,m,i));
            Karr2(ii) = -minf;
            ii = ii + 1;
        end;
        semilogy(tdb,Karr2,'x','Color','r');
        hold on;
        semilogy(tdb,Karr1,'x');
    end;

    
    K = s(t);
%     m = [1,2,4,8,16,32,64,128,256,512];
    m = [1,2,4,8,16,32];
    K1=[];
    for j = m
        K1 = [K1; multi(t,j)];    
    end;
    K1=[K; K1];
    figure()
    semilogy(tdb,K1,'-','LineWidth',3)
    return

%     Kns(0.01,1,0.1);

    
%     compare(t,512);
%     
%     
%     t1 = 10.^(-0.8);
%     m0 = eta.*t_b.*t1;
%     m0arr = 0:0.0001:1;
%     y = Kns(m0arr,4,t1)+1;
%     [minf,mini] = min(y)
%     plot(m0arr,y,'x')
    
    
%     %mn = fminsearch(@(x) Kns(x,512,10.^(-4)),m0)
%     %f1= -Kns(mn,512,10.^(-4));
%     %[maxmi,ind] = min(y)
%     %m0arr(ind)
%     %figure()
    

%x=-100:1:100;
%y=x.^2-x+4;
%figure()
%plot(x,y,'x')
%min(y)


end

