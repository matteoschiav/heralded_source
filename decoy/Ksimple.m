function output = Ksimple( mu,m,t,param )
%KSIMPLE compute key rate using the simplified statistics
%   from PRL A 88,023848 (2013)

    eta = param(1);
    t_b = param(2);
    p_d = param(3);
    V = param(4);
    f_EC = param(5);
    
    
    h = @(x) (-x.*log(x)-(1-x).*log(1-x))./log(2);
    A = @(mu,m) (1 - exp(-mu.*m)) ./ (1 - exp(-mu));
    P = @(mu,m,t) A(mu,m).*(1 - exp(-mu.*eta.*t_b.*t));
    Pd = @(mu,m,t) 2.*p_d.*(exp(-m.*mu) + A(mu,m).*(exp(-mu.*eta.*t_b.*t) - exp(-mu)) );
    R1ns = @(mu,m,t) P(mu,m,t) + Pd(mu,m,t) - (1-exp(-m.*mu)-A(mu,m).*mu.*exp(-mu));
    Rns = @(mu,m,t) P(mu,m,t) + Pd(mu,m,t);
    Q = @(mu,m,t) (0.5.*(1-V).*P(mu,m,t) + p_d) ./ Rns(mu,m,t);
    
    r1 = R1ns(mu,m,t);
    r = Rns(mu,m,t);
    q = Q(mu,m,t);
    q1 = q.*r./r1;
    ris = -(r1.*(1 - h(q1)) - f_EC.*r.*h(q));
    if ((r1<0) || (r1>1))
        output = 0;
    elseif ((q<0) || (q>0.5) || (q1<0) || (q1>0.5))
        output = 0;
    else
        output = ris;
    end;
    %if imag(ris) > 0
    %    out = 0;
    %else
    %    out = ris;
    %end;

end

