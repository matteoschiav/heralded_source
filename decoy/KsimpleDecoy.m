function output = KsimpleDecoy( mu,m,t,param )
%KSIMPLEDECOY compute key rate using the simplified statistics
%   from PRL A 88,023848 (2013) and the decoy state technique

    eta = param(1);
    t_b = param(2);
    p_d = param(3);
    V = param(4);
    f_EC = param(5);
    
    etac = eta.*t_b.*t;
    h = @(x) (-x.*log(x)-(1-x).*log(1-x))./log(2);
    A = @(mu,m) (1 - exp(-mu.*m)) ./ (1 - exp(-mu));
    P = @(mu,m,t) A(mu,m).*(1 - exp(-mu.*etac));
    Pd = @(mu,m,t) 2.*p_d.*(exp(-m.*mu) + A(mu,m).*(exp(-mu.*etac) - exp(-mu)) );
    R1ns = @(mu,m,t) A(mu,m).*mu.*exp(-mu).*(etac + 2.*p_d.*(1-etac));
    R0ns = @(mu,m) 2.*p_d.*exp(-m.*mu);
    Rns = @(mu,m,t) P(mu,m,t) + Pd(mu,m,t);
    Q = @(mu,m,t) (0.5.*(1-V).*P(mu,m,t) + p_d) ./ Rns(mu,m,t);
    Q1 = @(mu,m,t) (p_d + 0.5.*(1-V).*etac) ./ (etac + 2.*p_d.*(1-etac));
    
    p = P(mu,m,t);
    pd = Pd(mu,m,t);
    p0 = exp(-m.*mu);
    p1 = A(mu,m).*mu.*exp(-mu);
    r1 = R1ns(mu,m,t);
    r0 = R0ns(mu,m);
    r = Rns(mu,m,t);
    q = Q(mu,m,t);
    q1 = Q1(mu,m,t);
    ris = -(r0 + r1.*(1 - h(q1)) - f_EC.*r.*h(q));
    if ((r1<0) || (r1>1))
        output = 0;
    elseif ((q<0) || (q>0.5) || (q1<0) || (q1>0.5))
        output = 0;
    else
        output = ris;
    end;
    %if imag(ris) > 0
    %    out = 0;
    %else
    %    out = ris;
    %end;

end

