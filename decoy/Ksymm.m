function output = Ksymm( mu,m,t,param )
%KSYMM compute key rate using the symmetric heralded configuration
%   from PRL A 88,023848 (2013)

    eta = param(1);
    t_b = param(2);
    p_d = param(3);
    V = param(4);
    f_EC = param(5);
    
    etas = param(6);
    gammas = param(7);
    
    h = @(x) (-x.*log(x)-(1-x).*log(1-x))./log(2);    

    function out = P(mu,m,t)
        k = floor(log(m)./log(2));
        etac = eta.*t_b.*t;
        a1 = exp(-etas.*mu.*(2.^k)./(gammas.^k));
        s1 = (1 - exp(-etac.*mu.*(1-etas)));
        a2 = (1 - exp(-mu.*etas.*(2.^k)./(gammas.^k))) ./ (1 - exp(-mu.*etas./(gammas.^k)));
        s2 = (1 - exp(-mu.*etac) - exp(-etas.*mu./(gammas.^k)).*(1 - exp(-mu.*etac.*(1-etas))) );
        out = a1.*s1 + a2.*s2;
    end;

    function out = Pd(mu,m,t)
        k = floor(log(m)./log(2));
        etac = eta.*t_b.*t;
        a1 = exp(-etas.*mu.*(2.^k)./(gammas.^k) - etac.*mu.*(1-etas));
        s1 = 1;
        a2 = exp(-mu.*etac) .* (1 - exp(-etas.*mu.*(2.^k)./(gammas.^k))) ./ (1 - exp(-etas.*mu./(gammas.^k)));
        s2 = 1 - exp(-etas.*mu.*(1./(gammas.^k)-1) - mu.*etas.*(1-etac)); 
        out = 2.*p_d.*(a1.*s1 + a2.*s2);
    end;
    
    function out = pmag2(mu,m)
        k = floor(log(m)./log(2));
        p0 = exp(-(1-etas).*mu - etas.*mu.*(2.^k)./(gammas.^k)) + exp(-mu).*(1-exp(-etas.*mu.*(1./(gammas.^k)-1)))./(1-exp(-etas.*mu./(gammas.^k))).*(1-exp(-etas.*mu.*(2.^k)./(gammas.^k)));
        p1 = (1-etas).*mu.*exp(-(1-etas).*mu - etas.*mu.*(2.^k)./(gammas.^k)) + mu.*exp(-mu).*(1-(1-etas).*exp(-etas.*mu.*(1./(gammas.^k)-1)))./(1-exp(-etas.*mu./(gammas.^k))).*(1-exp(-etas.*mu.*(2.^k)./(gammas.^k)));
        out = 1 - p0 - p1;
    end;

    Rns = @(mu,m,t) (P(mu,m,t) + Pd(mu,m,t));
    R1ns = @(mu,m,t) (P(mu,m,t) + Pd(mu,m,t) - pmag2(mu,m));
    Q = @(mu,m,t) (0.5.*(1-V).*P(mu,m,t) + p_d) ./ Rns(mu,m,t);
    
    r1 = R1ns(mu,m,t);
    r = Rns(mu,m,t);
    q = Q(mu,m,t);
    q1 = q.*r./r1;
    ris = -(r1.*(1 - h(q1)) - f_EC.*r.*h(q));
    if ((r1<0) || (r1>1))
        output = 0;
    elseif ((q<0) || (q>0.12) || (q1<0) || (q1>0.12))
        output = 0;
    else
        output = ris;
    end;
    %if imag(ris) > 0
    %    out = 0;
    %else
    %    out = ris;
    %end;

end

