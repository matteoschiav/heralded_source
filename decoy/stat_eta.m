clear all
close all

init

set (0,'DefaultAxesFontSize',20);

etas = param(6);
gammas = param(7);
% mus = 0.003777;
% mua = 0.01;
mus = 0.003777;
mua = 0.008183;
mu = 0.005;
m = 32;
k = floor( log(m) ./ log(2) );

%% Asymmetric source mean
i = 1:m;
ki = i-1;
ki(m) = m-1;
    
Ai = exp(-mua.*etas.*(gammas.^(1-i)-1)./(1-gammas));
bi = exp(-etas.*mua.*(1./(gammas.^ki)-1));
mean_a = mua.*(1 + etas.*dot(Ai,bi)) + (1-etas).*mua.*exp(-mua.*etas.*((2-gammas).*gammas.^(1-m)-1)./(1-gammas))

%% Symmetric source mean
k = floor( log(m) ./ log(2) );

mean_s = mus.*(1 + etas./(1 - exp(-etas.*mus./(gammas.^k)))).*(1-exp(-etas.*mus.*(2.^k)./(gammas.^k))) + (1-etas).*mus.*exp(-mus.*etas.*(2.^k)./gammas.^k)

n = 0:3;


Ks = vec2mat(ps(n,mus,m,etas,gammas),1);
Ka = vec2mat(pa(n,mua,m,etas,gammas),1);
% Kp = vec2mat(poisspdf(n,mu),1);
K = [Ks,Ka]
bar(n,K,'grouped');

xlabel('n');
ylabel('P_n');

leg = cell(1,2);
leg{1} = 'SMHPS';
leg{2} = 'AMHPS';
legend(leg,'fontsize',20);


print -depsc mu_max.eps