function output = KsymmDecoy( mu,m,t,param )
%KSYMMDECOY compute key rate using the symmetric heralded configuration
%   from PRL A 88,023848 (2013) and the decoy state

    eta = param(1);
    t_b = param(2);
    p_d = param(3);
    V = param(4);
    f_EC = param(5);
    
    etas = param(6);
    gammas = param(7);
    
    h = @(x) (-x.*log(x)-(1-x).*log(1-x))./log(2);    
    k = floor(log(m)./log(2));
    etac = eta.*t_b.*t;

    function out = P(mu,m,t)
        a1 = exp(-etas.*mu.*(2.^k)./(gammas.^k));
        s1 = (1 - exp(-etac.*mu.*(1-etas)));
        a2 = (1 - exp(-mu.*etas.*(2.^k)./(gammas.^k))) ./ (1 - exp(-mu.*etas./(gammas.^k)));
        s2 = (1 - exp(-mu.*etac) - exp(-etas.*mu./(gammas.^k)).*(1 - exp(-mu.*etac.*(1-etas))) );
        out = a1.*s1 + a2.*s2;
    end;

    function out = Pd(mu,m,t)
        a1 = exp(-etas.*mu.*(2.^k)./(gammas.^k) - etac.*mu.*(1-etas));
        s1 = 1;
        a2 = exp(-mu) .* (1 - exp(-etas.*mu.*(2.^k)./(gammas.^k))) ./ (1 - exp(-etas.*mu./(gammas.^k))) .* exp(mu.*(1-etac));
        s2 = 1 - exp(-etas.*mu.*(1./(gammas.^k)-1) - mu.*etas.*(1-etac)); 
        out = 2.*p_d.*(a1.*s1 + a2.*s2);
    end;
    
    p1 = @(mu,m) (1-etas).*mu.*exp(-(1-etas).*mu - etas.*mu.*(2.^k)./(gammas.^k)) + mu.*exp(-mu).*(1-(1-etas).*exp(-etas.*mu.*(1./(gammas.^k)-1)))./(1-exp(-etas.*mu./(gammas.^k))).*(1-exp(-etas.*mu.*(2.^k)./(gammas.^k)));
    p0 = @(mu,m) exp(-(1-etas).*mu - etas.*mu.*(2.^k)./(gammas.^k)) + exp(-mu).*(1-exp(-etas.*mu.*(1./(gammas.^k)-1)))./(1-exp(-etas.*mu./(gammas.^k))).*(1-exp(-etas.*mu.*(2.^k)./(gammas.^k)));
    
    P1 = p1(mu,m);
    P0 = p0(mu,m);
    p = P(mu,m,t);
    pd = Pd(mu,m,t);
    Rns = @(mu,m,t) (P(mu,m,t) + Pd(mu,m,t));
    R1ns = @(mu,m,t) p1(mu,m).*(etac + 2.*p_d.*(1 - etac));
    R0ns = @(mu,m) p0(mu,m).*2.*p_d;
    Q = @(mu,m,t) (0.5.*(1-V).*P(mu,m,t) + p_d) ./ Rns(mu,m,t);
    Q1 = @(mu,m,t) (0.5.*(1-V).*etac + p_d) ./ (etac + 2.*p_d.*(1-etac));

    p = P(mu,m,t);
    pd = Pd(mu,m,t);
    p0 = p0(mu,m);
    p1 = p1(mu,m);
    r1 = R1ns(mu,m,t);
    r0 = R0ns(mu,m);
    r = Rns(mu,m,t);
    q = Q(mu,m,t);
    q1 = Q1(mu,m,t);
    ris = -(r0 + r1.*(1 - h(q1)) - f_EC.*r.*h(q));
    if ((r1<0) || (r1>1))
        output = 0;
    elseif ((q<0) || (q>0.5) || (q1<0) || (q1>0.5))
        output = 0;
    else
        output = ris;
    end;
    %if imag(ris) > 0
    %    out = 0;
    %else
    %    out = ris;
    %end;

end

