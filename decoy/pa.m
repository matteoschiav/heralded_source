function out = pa( n,mu,m,etas,gammas )
%PA_SIGNAL asymmetric statistics, for the "signal"

%     function ki = k(i)
%         if i < m-1
%             ki = i;
%         else
%             ki = m-1;
%         end;
%     end;

    i = 1:m;
    ki = 1:m-1;
    ki(m) = m-1;
    
    if gammas < 1
        s = ((1-etas).*mu).^n ./ factorial(n) .* exp(-(1-etas).*mu - etas.*mu.*((2-gammas).*gammas.^(1-m)-1)./(1-gammas));
    else
        s = ((1-etas).*mu).^n ./ factorial(n) .* exp(-(1-etas).*mu - etas.*mu.*m);
    end;
    for j = i
        A = mu.^n .* exp(-mu) ./ factorial(n) .* ( 1 - (1-etas).^n.*exp(-etas.*mu.*(1./(gammas.^ki(j))-1)) );
        if gammas < 1
            B = exp(-mu.*etas.*(gammas.^(1-j)-1)./(1-gammas));
        else
            B = exp(-mu.*etas.*(j-1));
        end;
        s = s + A.*B;
    end;
        
    out = s;
            

end

