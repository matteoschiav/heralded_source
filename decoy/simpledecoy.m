init

tdb=0:0.1:40;
t=10.^(-tdb/10);

% m = [2,4,8,16,32,64,128,256,512];
m = [8,128];

% K = optimsimple(t,m,param);
% K = [K; stdplot(t,param)];
% subplot(1,2,1);
% semilogy(tdb,K,'-','LineWidth',3);

leg = cell(1,length(m)+3);
ii = 1;
for i = m
%     leg{ii} = int2str(i);
    leg{ii} = num2str(i);
    ii = ii + 1;
end;
leg{ii} = 'single photon';
leg{ii+1} = 'WCS';
leg{ii+2} = 'Weak+Vacuum';

K1 = optimsimpledecoy(t,m,param);
K1 = [K1; Kstd];
% subplot(1,2,2);
semilogy(tdb,K1,'-','LineWidth',3);
legend(leg);

