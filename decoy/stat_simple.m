init

pn = @(n,x) (x.^n)./factorial(n).*exp(-x).*(1-exp(-m.*x))./(1-exp(-x)).*(1-kroneckerDelta(n)) + kroneckerDelta(n).*exp(-m.*x)

etas = param(6);
gammas = param(7);
mu = 0.45;
m = 2;
k = floor( log(m) ./ log(2) );

n = 0:10;

i = 1:m;
ki = i-1;
ki(m) = m-1;
    
mean = mu;
Kp = poisspdf(n,mean);
Ka2 = pn(n,mean);

Kp = vec2mat(Kp,1);
Ka = vec2mat(Ka,1);
K = [Kp, Ka];
bar(n,K,'grouped');

legend('poissonian','heralded asymmetric');