function [K1,mu1] = optimasymm( t,m,parm )
%OPTIMASYMM optimization of the key rate using the asymmetric statistics
%   INPUT: arrays of t and m
%   OUTPUT: array with the optimized key rates for each t and m

    eta = parm(1);
    t_b = parm(2);
    p_d = parm(3);
    V = parm(4);
    f_EC = parm(5);
    
    global optims


    % if method is absent or is 0, MATLAB minimization is used
    % if method is 1, the array is computed and the minimum is found
    %   by comparing the different values - VERY SLOW
    function [Karr,mumin] = multi(t,m,method)
        Karr = zeros(size(t));
        mumin = zeros(size(t));
        ii=1;
        for i = t
            if (nargin < 3) || (method == 0)
                if ii
                    mu0 = eta.*t_b.*i;
                else
                    mu0 = mumin(ii-1);
                end;
                [min,fmin] = fminsearch(@(x) Kasymm(x,m,i,parm),mu0,optims);
%                 Karr(ii) = -Kasymm(min,m,i,parm);
                Karr(ii) = -fmin;
                mumin(ii) = min;
                warning('m = %d , min = %f , L = %f , k = %f',m,min,-10.*log(i)./log(10),Karr(ii));
            else
                mu0 = eta.*t_b.*i;
                mu0arr = 0:0.000001:(mu0+0.0001);
                [minf,mini] = min(Kasymm(mu0arr,m,i,parm));
                Karr(ii) = -minf;
            end;
            ii=ii+1;
        end;
    end;

    K1 = [];
    mu1 = [];
    for j = m
        [K,mu] = multi(t,j);
        K1 = [K1; K];
        mu1 = [mu1; mu];
    end;

end

