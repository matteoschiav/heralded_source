function out = ps( n,mu,m,etas,gammas )
%PS_SIGNAL symmetric statistics, for the "signal"

    k = floor (log(m)./log(2));
    out = ((1-etas).*mu).^n ./ factorial(n) .* exp(-(1-etas).*mu - etas.*mu.*(2.^k)./(gammas.^k))  +  mu.^n .* exp(-mu) ./ factorial(n) .* ( 1 - (1-etas).^n .* exp(-etas.*mu.*(1./(gammas.^k)-1)) ) ./ (1 - exp(-etas.*mu./(gammas.^k))) .* (1 - exp(-etas.*mu.*(2.^k)./(gammas.^k)));


end

