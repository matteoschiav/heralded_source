function output = Kasymm( mu,m,t,param )
%KASYMM compute key rate using the symmetric heralded configuration
%   from PRL A 88,023848 (2013)

    eta = param(1);
    t_b = param(2);
    p_d = param(3);
    V = param(4);
    f_EC = param(5);
    
    etas = param(6);
    gammas = param(7);
    
    h = @(x) (-x.*log(x)-(1-x).*log(1-x))./log(2);
    etac = eta.*t_b.*t;

    k = 1:m-1;
    k(end+1)=m-1;
%     k
    ind = 1:m;
    if gammas < 1
        fg1 = ((2-gammas).*gammas.^(1-m)-1)./(1-gammas);
        fg2 = (gammas.^(1-ind)-1)./(1-gammas);
    else
        fg1 = m;
        fg2 = ind-1;
    end;
%     fg1
%     fg2

    function out = P(mu,m,t)
        a1 = exp(-etas.*mu.*fg1);
        s1 = (1 - exp(-etac.*mu.*(1-etas)));
        s = 0;
        for j = 1:m
            a2 = exp(-mu.*etas.*fg2(j));
            s2 = (1 - exp(-mu.*etac) - exp(-etas.*mu./(gammas.^k(j))).*(1 - exp(-mu.*etac.*(1-etas))) );
            s = s + a2.*s2;
        end;
        out = a1.*s1 + s;
    end;

    function out = Pd(mu,m,t)
        a1 = exp(-etas.*mu.*fg1 - etac.*mu.*(1-etas));
        s = 0;
        for j = 1:m
            a2 = exp( -mu.*etac -mu.*etas.*fg2(j) );
            s2 = 1 - exp(etas.*mu - etas.*mu./(gammas.^k(j)) - mu.*etas.*(1-etac));
            s = s + a2.*s2;
        end;
        out = 2.*p_d.*(a1 + s);
    end;
    
    function out = p0(mu,m)
        out = exp(-(1-etas).*mu - etas.*mu.*fg1);
        for i = 1:m
            out = out + exp(-mu -mu.*etas.*fg2(i)).*(1 - exp(etas.*mu - etas.*mu./(gammas.^k(i))) );
        end;
    end;
    
    function out = p1(mu,m)
        out = (1-etas).*mu.*exp(-(1-etas).*mu - etas.*mu.*fg1);
        for i = 1:m
            out = out + mu.*exp(-mu).*exp(-etas.*mu.*fg2(i)).*(1 - (1-etas).*exp(etas.*mu - etas.*mu./(gammas.^k(i))));
        end;
    end;
    

    Rns = @(mu,m,t) (P(mu,m,t) + Pd(mu,m,t));
    R1ns = @(mu,m,t) p1(mu,m).*(etac + 2.*p_d.*(1-etac));
    R0ns = @(mu,m,t) p0(mu,m).*2.*p_d;
    Q = @(mu,m,t) (0.5.*(1-V).*P(mu,m,t) + p_d) ./ Rns(mu,m,t);
    Q1 = @(mu,m,t) (0.5.*(1-V).*etac + p_d) ./ (etac + 2.*p_d.*(1-etac));
    
    r1 = R1ns(mu,m,t);
    r = Rns(mu,m,t);
    r0 = R0ns(mu,m);
    q = Q(mu,m,t);
    q1 = Q1(mu,m,t);
    ris = -(r0 + r1.*(1 - h(q1)) - f_EC.*r.*h(q));
    if ((r1<0) || (r1>1))
        output = 0;
    elseif ((q<0) || (q>0.5) || (q1<0) || (q1>0.5))
        output = 0;
    else
        output = ris;
    end;
    %if imag(ris) > 0
    %    out = 0;
    %else
    %    out = ris;
    %end;

end

