function output = stdplot( t,parm )
%STDPLOT calcola il key rate per il singolo fotone e la weak coherent
%source

    eta = parm(1);
    t_b = parm(2);
    p_d = parm(3);
    V = parm(4);
    f_EC = parm(5);
    
    % entropia binaria
    h = @(x) (-x.*log(x)-(1-x).*log(1-x))./log(2);

    % curva per il singolo fotone
    function out = s(t)
        % sono i P e i Pd usati da Scarani a pag. 1336
        % il gain � dato da P+Pd (nel mio testo uso Q)
        P = eta.*t_b.*t;
  		Pd = 2.*p_d.*(1-eta.*t_b.*t);
        % Q � il QBER (nel mio testo uso E)
 		Q = (0.5.*(1-V).*P + p_d) ./ (P + Pd);
        % out � il secret key rate (K in Scarani, R nel mio testo)
        out = (P + Pd).*(1-(1+f_EC).*h(Q));
    end;
    
    K = [];
    K = [K; s(t)];
    K = [K; optimsimple(t,[1],parm)];
%     K = [K; optimsimpledecoy(t,[1],parm)];
    
    output = K;
    
end
