clear all
close all

init

tdb=0:0.1:55;
t=10.^(-tdb/10);

set (0,'DefaultAxesFontSize',20);

h = 0.1:0.3:1;

colors = get(0,'DefaultAxesColorOrder');
colors = [colors; [0.5 0.5 0.5]; [1 1 0]; [0 0.75 0]];
set (0,'DefaultAxesColorOrder',colors);
% m = [2,4,8,16,32,64,128,256,512];
% m = [2,4,8,32,128,512];
m = 32;

% K = optimsimple(t,m,param);
% K = [K; stdplot(t,param)];
% subplot(1,2,1);
% semilogy(tdb,K,'-','LineWidth',3);

leg = cell(1,length(h)+3);
ii = 1;
for i = h
%     leg{ii} = int2str(i);
    leg{ii} = strcat('\eta=',num2str(i));
    ii = ii + 1;
end;
leg{ii} = 'single photon';
leg{ii+1} = 'WCS';
leg{ii+2} = 'WCS + infinite decoy';

if not (exist('symm_eta.mat','file'))
    K1 = [];
    mu1 = [];
    for i = h
        param(6) = i;
        [k,mu] = optimsymm(t,m,param);
        K1 = [K1; k];
        mu1 = [mu1; mu];
    end;    
    K1 = [K1; Kstd];
    save('symm_eta.mat','K1','mu1');
else
    load('symm_eta.mat','K1','mu1');
end;

%% Poissonian case => SMHPS with m=1 gammas=1 etas=1
if not (exist('poissonian_mu.mat','file'))
    [kp, mup] = optimsymm(t,1,[param(1:5),1,1]);
    save('poissonian_mu.mat','kp','mup');
else
    load('poissonian_mu.mat','kp','mup');
end


%%

% K1 = optimsymm(t,m,param);
% K1 = [K1; Kstd];
% subplot(1,2,2);
figure('position',[0 0 800 800]);
lk1 = length(K1(:,1));
semilogy(tdb,K1(1:lk1-3,:),'-','LineWidth',3);
hold on
semilogy(tdb,K1(lk1-2,:),':','color','black','LineWidth',2);
cols = lines;
for i = 1:2
    semilogy(tdb,K1(lk1-2+i,:),'--','LineWidth',2,'color',cols(i,:));
end
set(gca,'ColorOrder',colors);
legend(leg,'fontsize',14);
title('SMHPS with no decoy \gamma = 0.5');
xlabel('Losses L [dB]');
ylabel('Key rate R');
ylim([2e-6 .5]);
xlim([0 55]);

%print -depsc symm_eta.eps

% graph of the mu
legmu = cell(1,length(h)+1);
ii = 1;
for i = h
%     leg{ii} = int2str(i);
    legmu{ii} = strcat('\eta=',num2str(i));
    ii = ii + 1;
end;
legmu{ii} = 'WCS';

mu1 = [mu1;mup];
figure()
plot(tdb,mu1,'-','LineWidth',2);
legend(legmu,'fontsize',14);
xlabel('Losses L [dB]');
ylabel('\mu');


%% graphs of the P0, P1 and PM
% gammas = param(7);
% 
% figure()
% p0 = [];
% p1 = [];
% pm = [];
% for i = 1:length(h)
%     ps0 = ps(0,mu1(i,:),m,h(i),gammas);
%     ps1 = ps(1,mu1(i,:),m,h(i),gammas)
%     p0 = [p0; ps0];
%     p1 = [p1; ps1];
%     pm = [pm; 1-ps0-ps1];
% end
% 
% p0 = [p0; ps(0,mup,1,1,1)];
% p1 = [p1; ps(1,mup,1,1,1)];
% pm = [pm; 1 - ps(0,mup,1,1,1) - ps(1,mup,1,1,1)];
% 
% figure();
% plot(tdb,p0,'-','LineWidth',2);
% legend(legmu,'fontsize',14);
% xlabel('Losses L [dB]');
% ylabel('P_0');
% 
% figure()
% plot(tdb,p1,'-','LineWidth',2);
% legend(legmu,'fontsize',14);
% xlabel('Losses L [dB]');
% ylabel('P_1');
% 
% figure()
% plot(tdb,pm,'-','LineWidth',2);
% legend(legmu,'fontsize',14);
% xlabel('Losses L [dB]');
% ylabel('P_M');
