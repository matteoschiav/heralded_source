clear all;
close all;


param = [];

param(1) = 0.1; %eta
param(2) = 1; %t_b
param(3) = 1e-5; %p_d
param(4)= 0.99; %V
param(5) = 1.2; %f_EC

param(6) = 0.7; %etas
param(7) = 0.5; %gammas

tdb=0:0.1:40;
t=10.^(-tdb/10);

m = [1,2,4,8,16,32,64,128,256];
% m = [1,10,200,1000];
% m = [1,32,256,1024];
m = [4,512];
% m = [1,8,32,128,1024];

tdb=0:0.1:40;
t=10.^(-tdb/10);

% Kasymm(0.01,200,10.^(-1),param);
% Ksymm(0.01,4,10.^(-1),param);

% K = optimsimple(t,m,param);
% K = [K; stdplot(t,param)];
% subplot(1,2,1);
% semilogy(tdb,K,'-','LineWidth',3);

K1 = [];
% K1 = [K1; optimasymm(t,m,param)];
% K1 = [K1; optimsymm(t,m,param)];
% K1 = [K1; optimsimple(t,m,param)];
% K1 = [K1; optimsimpledecoy(t,m,param)];
K1 = [K1; optimsymmdecoy(t,m,param)];
K1 = [K1; optimasymmdecoy(t,m,param)];
K1 = [K1; stdplot(t,param)];


% mu = 0.001:0.0001:10;
% for i=mu
%     K1 = [K1 -KsymmDecoy(i,2,0.01,param)];
% end;
% figure()
% plot(mu,K1,'x');

% KsymmDecoy(0.001,2,0.01,param)
% KsimpleDecoy(0.001,2,0.01,param)



% subplot(1,2,2);
figure();
semilogy(tdb,K1,'-','LineWidth',3);
% semilogy(tdb,K1,'x');