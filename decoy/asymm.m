init

tdb=0:0.1:55;
t=10.^(-tdb/10);

set (0,'DefaultAxesFontSize',20);

% m = [2,4,8,16,32,64,128,256,512];
% m = [2,4,8,32,128,512];
m = [2,4,8,32,128]; % per l'articolo!
% m = 32;

% K = optimsimple(t,m,param);
% K = [K; stdplot(t,param)];
% subplot(1,2,1);
% semilogy(tdb,K,'-','LineWidth',3);

leg = cell(1,length(m)+3);
ii = 1;
for i = m
%     leg{ii} = int2str(i);
    leg{ii} = strcat('m=',num2str(i));
    ii = ii + 1;
end;
leg{ii} = 'single photon';
leg{ii+1} = 'WCS';
leg{ii+2} = 'WCS + infinite decoy';

if not (exist('asymm.mat','file'))
    K1 = optimasymm(t,m,param);
    K1 = [K1; Kstd];
    save('asymm.mat','K1');
else
    load('asymm.mat','K1');
end;

% K1 = optimasymm(t,m,param);
% K1 = [K1; Kstd];
% subplot(1,2,2);
figure('position',[0 0 700 700]);
lk1 = length(K1(:,1));
semilogy(tdb,K1(1:lk1-3,:),'-','LineWidth',3);
hold on
semilogy(tdb,K1(lk1-2,:),':','color','black','LineWidth',2);
cols = lines;
for i = 1:2
    semilogy(tdb,K1(lk1-2+i,:),'--','LineWidth',2,'color',cols(i,:));
end
legend(leg,'fontsize',14);
title('AMHPS with no decoy');
xlabel('Losses L [dB]');
ylabel('Key rate R');
ylim([8e-8 .5]);
xlim([0 55]);

% print -depsc asymm.eps
