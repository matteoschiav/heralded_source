% script di inizializzazione (richiamato all'inizio degli altri script)

% crea il vettore di parametri della simulazione
param = zeros(1,7);

% parametri della simulazione. I valori sono �resi da Scarani, pag. 1337
% param(1) = 0.1; %eta -> efficienza del rivelatore di Bob
param(1) = 0.25; % vedi id220 della idQuantique
param(2) = 1; %t_b -> trasmissivit� del sistema di Bob
% param(3) = 1e-5; % p_d -> probabilit� di un dark count per un rivelatore: dato che Bob usa 2 rivelatori, si ha Y_0 = 1-(1-p_d)^2
param(3) = 2e-7; % vedi id220 della idQuantique
param(4) = 0.99; %V -> visibilit� del canale (l'errore che un fotone colpisca il detector sbagliato � (1-V)/2)
param(5) = 1.05; %f_EC -> efficienza di error correction

param(6) = 0.7; %etas -> efficienza del meccanismo di heralding (l'eta dell'articolo di Mazzarella)
param(7) = 0.5; %gammas -> la trasmissivit� dello switch ottico (gamma nell'articolo di Mazzarella)

% set (0,'DefaultAxesColorOrder',rand(15,3));

% vettore delle perdite del sistema (tdb in dB)
tdb = 0:0.1:55;
% trasmissivit� del canale
t = 10.^(-tdb./10);

% disegna le curve di rate per il singolo fotone, il weak coherent pulse
% (in stdplot()) e per il Weak+Vacuum decoy state (keyrate_wv())
if not (exist('stdplot.mat','file'))
    addpath('../curtis');
    Kstd = stdplot(t,param);
    Kstd = [Kstd; keyrate_wv(t,param)];
    save('stdplot.mat','Kstd');
else
    load('stdplot.mat','Kstd');
end;

% if not (exist('stdplot_nv.mat','file'))
%     Knv = keyrate_nv(t,param);
%     save('stdplot_nv.mat','Knv');
% else
%     load('stdplot_nv.mat','Knv');
% end;

global mopt
mopt = [];


% change optimization parameters
global optims

optims = optimset('fminsearch');
optims.TolFun = 1e-7;
optims.TolX = 1e-7;