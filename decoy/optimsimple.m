function output = optimsimple( t,m,parm )
%OPTIMSIMPLE optimization of the key rate using the simple statistics MHPS
%   INPUT: arrays of t and m
%   OUTPUT: array with the optimized key rates for each t and m

    eta = parm(1);
    t_b = parm(2);
    p_d = parm(3);
    V = parm(4);
    f_EC = parm(5);
    
    global optims
    
    function out = multi(t,m,method)
        Karr = zeros(size(t));
        ii=1;
        for i = t
            if (nargin < 3) || (method == 0)
                if ii
                    mu0 = eta.*t_b.*i;
                else
                    mu0 = min0;
                end;
                min0 = fminsearch(@(x) Ksimple(x,m,i,parm),mu0,optims);
                Karr(ii) = -Ksimple(min0,m,i,parm);
                warning('m = %d , min = %f',m,min0./mu0);
            else
                mu0 = eta.*t_b.*i;
                mu0arr = 0:0.000001:(mu0+0.0001);
                [minf,mini] = min(Ksimple(mu0arr,m,i,parm));
                Karr(ii) = -minf;
            end;
            ii=ii+1;
        end;
        out = Karr;
    end;

    K1 = [];
    for j = m
        K1 = [K1; multi(t,j)];
    end;
    
    output = K1;

end

