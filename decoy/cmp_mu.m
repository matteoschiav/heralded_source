init

mu = 0:0.0001:0.02;
t = 10^(-0.9);
m = 32;

set (0,'DefaultAxesFontSize',20);

Ks = [];
Ka = [];

for i = mu
    Ks = [Ks; max(-Ksymm(i,m,t,param),0)];
    Ka = [Ka; max(-Kasymm(i,m,t,param),0)];
end


K = [Ks, Ka];

figure();
plot(mu,K,'LineWidth',3);

xlabel('\mu');
ylabel('Key rate R');

leg = cell(1,2);
leg{1} = 'SMHPS';
leg{2} = 'AMHPS';
legend(leg,'fontsize',20);

print -depsc mu_opt.eps
