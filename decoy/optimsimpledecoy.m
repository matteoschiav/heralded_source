function output = optimsimpledecoy( t,m,parm )
%OPTIMSIMPLEDECOY optimization of the key rate using the simple statistics
%   INPUT: arrays of t and m
%   OUTPUT: array with the optimized key rates for each t and m

    eta = parm(1);
    t_b = parm(2);
    p_d = parm(3);
    V = parm(4);
    f_EC = parm(5);

    h = @(x) (-x.*log(x)-(1-x).*log(1-x))./log(2);

    % if method is absent or is 0, MATLAB minimization is used
    % if method is 1, the array is computed and the minimum is found
    %   by comparing the different values - VERY SLOW
    function out = multi(t,m,method)
        Karr = zeros(size(t));
        ii=1;
        for i = t
            if (nargin < 3) || (method == 0)
                etac = eta.*t_b.*i;
%                 e1 = (p_d + 0.5.*(1-V).*etac) ./ (etac + p_d.*(1-etac));
%                 he1 = h(e1);
%                 mu0 = he1./(he1.*(m+1)-1);
%                 mu0 = he1;
                mu0 = 1;
                min = fminsearch(@(x) KsimpleDecoy(x,m,i,parm),mu0);
                Karr(ii) = -KsimpleDecoy(min,m,i,parm);
                warning('m = %d , min = %f',m,min/mu0);
%                 global mopt;
%                 mopt = [mopt; min/mu0];
            else
                mu0 = eta.*t_b.*i;
                mu0arr = 0:0.000001:(mu0+0.0001);
                [minf,mini] = min(KsimpleDecoy(mu0arr,m,i,parm));
                Karr(ii) = -minf;
            end;
            ii=ii+1;
        end;
        out = Karr;
    end;

    K1 = [];
    for j = m
        K1 = [K1; multi(t,j)];
    end;
    
    output = K1;

end

