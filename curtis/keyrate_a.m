function out = keyrate_a( t,m,parm,mufix )
%KEYRATE_A computes the key rate in the asymmetric case

    eta = parm(1);
    t_b = parm(2);
    p_d = parm(3);
    V = parm(4);
    f_EC = parm(5);
    
    gammas = parm(7);
    
    ed = (1-V)./2;
    
    h = @(x) (-x.*log(x) - (1-x).*log(1-x))./(log(2));
    
    out = [];
    jj=1;
    for j = m
%         k = floor(log(j)./log(2));
%         if (gammas.^k > 0)
%             mu0 = 0.5 - f_EC./(1-(gammas.^k)).*h(ed)./(1-h(ed));    
%         else
%             mu0 = 1 + f_EC.*h(ed)./(1-h(ed));
%         end;
%         min = max(mu0,0);
% %         min = mu0;
%         warning('m = %d ; mu0 = %f',j,min);
        warning('m = %d',j);

        Karr = zeros(size(t));
        ii=1;
        for i = t
%             etac = eta.*t_b.*i;
%             e1 = (p_d + 0.5.*(1-V).*etac) ./ (etac + p_d.*(1-etac));
%             he1 = h(e1);
%             warning('m = %d ; mu0 = %f',j,min);

%             mu0 = he1;
%             min = he1;
%             min = fminbnd(@(x) Ra(x,j,i,parm),0,0.1);
%             warning('%f',min);
%             min = fmincon(@(x) Rs(x,j,i,parm),mu0,-1,0);
            Karr(ii) = -Ra(mufix(jj),j,i,parm);
%             warning('WARNING Parameters: K = %f ; min = %f ; m = %d ; t = %f ;', Karr(ii), min, j, i);
            ii = ii + 1;
        end;
        out = [out; Karr];
        jj = jj + 1;
    end;

end

