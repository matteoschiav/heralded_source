function out = Ra( mu,m,t,param)
%RA normalized rate in the case of asymmetric source
% La chiave � estratta separatamente dal caso "Fotone rivelato" e quello
% "Fotone non rivelato", dando due diversi secret key rate. Il secret key
% rate totale � dato da P_"Fotone rivelato"*Rate_"Fotone rivelato" +
% P_"Fotone non rivelato"*Rate_"Fotone non rivelato".

    eta = param(1);
    t_b = param(2);
    p_d = param(3);
    V = param(4);
    f_EC = param(5);
    
    etas = param(6);
    gammas = param(7);
    
    e0 = 0.5;
    ed = (1-V)./2;
    Y0 = 2.*p_d;
    etac = eta.*t_b.*t;
    
    i = 1:m;
    ki = i-1;
    ki(m) = m-1;
    
    
    function o = h(x)
        if (x == 0)
            o = 0;
        else
            o = (-x.*log(x)-(1-x).*log(1-x))./log(2);
        end;
    end;

%     h = @(x) (-x.*log(x)-(1-x).*log(1-x))./log(2);

    if (gammas < 1)
        Qs = 0;
        for j = i
            Ai = exp(-mu.*etas.*(gammas.^(1-j)-1)./(1-gammas)) ./ ( 1 - exp(-mu.*etas.*((2-gammas).*gammas.^(1-m)-1)./(1-gammas)) );
            bi = 1 - exp(-mu.*etac) - exp(-mu.*etas./(gammas.^ki(j))).*(1-exp(-mu.*etac.*(1-etas))) + Y0.*(exp(-mu.*etac) - exp(-mu.*etac.*(1-etas)-mu.*etas./(gammas.^ki(j))));
            Qs = Qs + Ai.*bi;
        end;
        s = 0;
        for j = i
            Ai = exp(-mu.*etas.*(gammas.^(1-j)-1)./(1-gammas)) ./ ( 1 - exp(-mu.*etas.*((2-gammas).*gammas.^(1-m)-1)./(1-gammas)) );
            ci = 1 - exp(-mu.*etac) - exp(-mu.*etas./(gammas.^ki(j))).*(1-exp(-mu.*etac.*(1-etas)));
            s = s + Ai.*ci;
        end;
        Es = (   e0.*Y0 + ed .* s   ) ./ Qs;
    else
        Qs = 0;
        for j = i
            Ai = exp(-mu.*etas.*(j-1)) ./ ( 1 - exp(-mu.*etas.*m) );
            bi = 1 - exp(-mu.*etac) - exp(-mu.*etas./(gammas.^ki(j))).*(1-exp(-mu.*etac.*(1-etas))) + Y0.*(exp(-mu.*etac) - exp(-mu.*etac.*(1-etas)-mu.*etas./(gammas.^ki(j))));
            Qs = Qs + Ai.*bi;
        end;
        s = 0;
        for j = i
            Ai = exp(-mu.*etas.*(j-1)) ./ ( 1 - exp(-mu.*etas.*m) );
            ci = 1 - exp(-mu.*etac) - exp(-mu.*etas./(gammas.^ki(j))).*(1-exp(-mu.*etac.*(1-etas)));
            s = s + Ai.*ci;
        end;
        Es = (   e0.*Y0 + ed .* s   ) ./ Qs;
    end;
    
    Qd = 1 - (1-Y0).*exp(-mu.*etac.*(1-etas));
    Ed = (   e0.*Y0 + ed.*(1-exp(-mu.*etac.*(1-etas)))   ) ./ Qd;
    
    if (gammas < 1)
        pd = exp(-etas.*mu.*((2-gammas).*gammas.^(1-m)-1)./(1-gammas));
    else
        pd = exp(-etas.*mu.*m);
    end;
    
    p0s = pa_signal(0,mu,m,etas,gammas);
    p1s = pa_signal(1,mu,m,etas,gammas);
    p2s = pa_signal(2,mu,m,etas,gammas);
    p0d = p_decoy(0,mu,etas);
    p1d = p_decoy(1,mu,etas);
    p2d = p_decoy(2,mu,etas);
    
%   Y0 estimated from parameters (One decoy state)    
    Y0U = min( [(Qs.*Es./(p0s.*e0)), (Qd.*Ed./(p0d.*e0))] );
    Y0L = max( [((p1s.*Qd - p1d.*Qs)./(p1s.*p0d - p1d.*p0s)), 0] );
    Y1L = max( [((p2s.*Qd - p2d.*Qs - Y0U.*(p2s.*p0d - p2d.*p0s))./(p2s.*p1d-p2d.*p1s)),0] );
    e1U = min( [((Qs.*Es - p0s.*Y0L.*e0)./(p1s.*Y1L)), ((p0d.*Qs.*Es - p0s.*Qd.*Ed)./((p0d.*p1s - p0s.*p1d).*Y1L))] );
    e1U = min( [((Qd.*Ed - p0d.*Y0L.*e0)./(p1d.*Y1L)),e1U] );
    
%   Y0 estimated directly from vacuum (Vacuum + Decoy state)
%     Y1L = max( [((p2s.*Qd - p2d.*Qs - Y0.*(p2s.*p0d - p2d.*p0s))./(p2s.*p1d-p2d.*p1s)),0] );
%     e1U = min( [((Qs.*Es - p0s.*Y0.*e0)./(p1s.*Y1L)), ((p0d.*Qs.*Es - p0s.*Qd.*Ed)./((p0d.*p1s - p0s.*p1d).*Y1L))] );
%     e = ((Qd.*Ed - p0d.*Y0.*e0)./(p1d.*Y1L));
%     e1U = min( [((Qd.*Ed - p0d.*Y0.*e0)./(p1d.*Y1L)),e1U] );

    
    if (e1U < 0) || (e1U > 0.5) || (Es < 0) || (Es > 0.5)
        out = 0;
    elseif (Y1L < 0) || (Y1L > 1)
        out = 0;
    else
%       One decoy state        
        out = - (p0s.*Y0L + p1s.*Y1L.*(1 - h(e1U)) - Qs.*f_EC.*h(Es)).*(1-pd) - (p0d.*Y0L + p1d.*Y1L.*(1-h(e1U)) - Qd.*f_EC.*h(Ed)).*pd;
%       Decoy + Vacuum
%         out = - (p0s.*Y0 + p1s.*Y1L.*(1 - h(e1U)) - Qs.*f_EC.*h(Es)).*(1-pd) - (p0d.*Y0 + p1d.*Y1L.*(1-h(e1U)) - Qd.*f_EC.*h(Ed)).*pd;
    end;
end

