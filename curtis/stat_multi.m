clear all
close all

init

etas = param(6);
gammas = param(7);
mus = [0.3,0.25,0.3,0.45,0.5,0.5,0.5,0.5,0.5,0.5];
mua = [0.25,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2];

e = 1:7;
m = 2.^e;
n = 0:4;

ps = [];
pa = [];

for i = 1:length(m)
    ps = [ps, vec2mat(ps_signal(n,mus(i),m(i),etas,gammas),1)];
    pa = [pa, vec2mat(pa_signal(n,0.2,m(i),etas,gammas),1)];
end

poiss = vec2mat(poisspdf(n,0.8625),1);

pt = [ps,pa,poiss]

bar(n,pt,'grouped')
