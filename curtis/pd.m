clear all
close all

init

etas = 0.7;
gammas = 0.5;

mus = 0.5;
mua = 0.2;

e = 1:10;
m = 2.^e;
% m = 1:1024;
k = floor(log(m)./log(2));

pds = exp(-etas.*mus.*(2.^k)./(gammas.^k));
pda = exp(-etas.*mua.*((2-gammas).*gammas.^(1-m)-1)./(1-gammas));

pdtot = [pds; pda]

plot(e,pdtot,'LineWidth',3);