clear all;
close all;

init

set (0,'DefaultAxesFontSize',20);

colors = get(0,'DefaultAxesColorOrder');
colors = [colors; [0.5 0.5 0.5]; [1 1 0]; [0 0.75 0]];
set (0,'DefaultAxesColorOrder',colors);
% m = [2,4,8,16,32,64,128,256,512];
% m = [2,3,4,8,10,50,100,500];
% m = [500,500,500,500,500];
% m = 50;
m = [2,4,8,32];

% NEW FUNCTION
% FOR etas = 0.7, gammas = 0.5
% m = 2; mu = 0.25;
% m = 3; mu = 0.2;
% m = 4; mu = 0.2;
% m = 50; mu = 0.2;
% m = 100; mu = 0.2;
% m = 500; mu = 0.2;

% mf = [0.1,0.3,0.5,0.75,1.];
% mf = [0.2,0.25,0.3,0.35,0.4];
% mf = [0.25,0.2,0.2,0.2,0.2,0.2,0.2,0.2];
% mf = [0.7,0.7,0.72,0.2,0.2,0.2,0.2,0.2];
% mf = 0.3;
mf = [0.25 0.2 0.2 0.2 0.2];

% m = 2;
% mf = 0.2;

if not(exist('asymmdecoy.mat','file'))
    K = keyrate_a(t,m,param,mf);
    save('asymmdecoy.mat','K');
else
    load('asymmdecoy.mat','K');
end

Ktot = [K; Kstd; Knv];

leg = cell(1,length(m)+4);
ii = 1;
for i = m
    leg{ii} = num2str(i);
    ii = ii + 1;
end;
leg{ii} = 'single photon';
leg{ii+1} = 'WCS';
leg{ii+2} = 'WCS + infinite decoy';
leg{ii+3} = 'WCS + one decoy';

figure('position',[0 0 700 700]);
lk1 = length(Ktot(:,1));
semilogy(tdb,Ktot(1:lk1-4,:),'-','LineWidth',3);
hold on
semilogy(tdb,Ktot(lk1-3,:),':','color','black','LineWidth',2);
cols = lines;
for i = 1:3
    semilogy(tdb,Ktot(lk1-3+i,:),'--','LineWidth',2,'color',cols(i,:));
end
legend(leg,'fontsize',14);
title('AMHPS with passive decoy');
xlabel('Losses L [dB]');
ylabel('Key rate R');
ylim([1e-8 .5]);
xlim([0 55]);

% print -depsc asymmdecoy.eps