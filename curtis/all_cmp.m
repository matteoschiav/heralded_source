% compares the active AMHPS and SMHPS and the passive ones for m=4
close all
clear all

colors = get(0,'DefaultAxesColorOrder');
colors = [colors; [0.5 0.5 0.5]; [1 1 0]; [0 0.75 0]];
set (0,'DefaultAxesColorOrder',colors);
set (0,'DefaultAxesFontSize',20);

init

global mopts
global mopta


m = 8;

if not(exist('all_cmp8.mat','file'))
    addpath('../decoy');
    K = [optimsymmdecoy(t,m,param); optimasymmdecoy(t,m,param)];
    mopts(mopts(:)<0.3) = 0.3;
    mopta(mopta(:)<0.2) = 0.2;
    K = [K; keyrate_s(t,m,param,mopts); keyrate_a(t,m,param,mopta)];
    save('all_cmp8.mat','K');
else
    load('all_cmp8.mat','K');
end

Ktot = [K; Kstd; Knv];

leg = cell(1,8);
leg{1} = 'active SMHPS';
leg{2} = 'active AMHPS';
leg{3} = 'passive SMHPS';
leg{4} = 'passive AMHPS';
leg{5} = 'single photon';
leg{6} = 'WCS';
leg{7} = 'WCS + infinite decoy';
leg{8} = 'WCS + one decoy';

figure('position',[0 0 700 700]);
semilogy(tdb,K,'LineWidth',3);
hold on
semilogy(tdb,Kstd(1,:),':','LineWidth',2,'color','black');
cols = lines;
for i = 1:2
    semilogy(tdb,[Kstd(1+i,:)],'--','LineWidth',2,'color',cols(i,:));
end
semilogy(tdb,Knv,'--','LineWidth',2,'color',cols(3,:));
legend(leg,'fontsize',14);

title('SMHPS vs AMHPS with decoy');
xlabel('Losses L [dB]');
ylabel('Key rate R');
ylim([4e-8 .5]);
xlim([0 55]);

% print -depsc 'all_cmp8.eps'