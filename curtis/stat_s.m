init

etas = param(6);
gammas = param(7);
mu = 0.003778;
m = 32;
k = floor( log(m) ./ log(2) );

n = 0:10;
mean = mu.*(1 + etas./(1 - exp(-etas.*mu./(gammas.^k))))
Kp = poisspdf(n,mean);
Ks = pa_signal(n,mu,m,etas,gammas);

Kp = vec2mat(Kp,1);
Ks = vec2mat(Ks,1);
K = [Kp, Ks];
bar(n,K,'grouped');

legend('poissonian','heralded symmetric');