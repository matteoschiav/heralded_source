function out = keyrate_s( t,m,parm,mufix )
%KEYRATE_S computes the key rate in the symmetric case

    eta = parm(1);
    t_b = parm(2);
    p_d = parm(3);
    V = parm(4);
    f_EC = parm(5);
    
    etas = parm(6);
    gammas = parm(7);
    
    ed = (1-V)./2;
    
    h = @(x) (-x.*log(x) - (1-x).*log(1-x))./(log(2));
    
    out = [];
    jj = 1;
    for j = m
%         k = floor(log(j)./log(2));
%         if (gammas.^k > 0)
%             mu0 = 0.5 - f_EC./(1-(gammas.^k)).*h(ed)./(1-h(ed));    
%         else
%             mu0 = 1 + f_EC.*h(ed)./(1-h(ed));
%         end;
%         min = max(mu0,0);
%         min = 0.65;
% %         min = mu0;

%         k = floor(log(j)./log(2));
%         hed = h(ed);
%         F = @(x) (f_EC.*hed.*x.*(1 + etas./(1-exp(-etas.*x./(gammas.^k)))) - ps_signal(1,x,j,etas,gammas).*(1-hed));
%         min = fminsearch(F,10);
%         warning('m = %d ; mu0 = %f',j,min);
%         warning('m = %d',j);


        Karr = zeros(size(t));
        ii=1;
        for i = t
%             mu0 = he1;
%             min = he1;
%             min = fminbnd(@(x) Rs(x,j,i,parm),0,1);
%             min = fmincon(@(x) Rs(x,j,i,parm),0,-1,0);
            Karr(ii) = -Rs(mufix(jj),j,i,parm);
%             warning('WARNING Parameters: K = %f ; min = %f ; m = %d ; t = %f ;', Karr(ii), min, j, i);
            ii = ii + 1;
        end;
        out = [out; Karr];
        jj = jj + 1;
    end;

end

