function out = keyrate_wv( t,parm )
%KEYRATE_WV computes the key rate for the Weak+Vacuum case

    eta = parm(1);
    t_b = parm(2);
    p_d = parm(3);
    V = parm(4);
    f_EC = parm(5);
    
    global optims

%     eta = 0.045;
%     t_b = 1;
%     p_d = 0.85e-6;
%     V = 0.934;
%     f_EC = 1;
        
    ed = (1-V)./2;
    
    h = @(x) (-x.*log(x) - (1-x).*log(1-x))./(log(2));
    
    syms x
    a = f_EC*h(ed)/(1-h(ed));
    mu0 = double(vpasolve((1-x)*exp(-x)-a,0.5));
    
    out = [];
    Karr = zeros(size(t));
    ii=1;
    for i = t
        nmin = fminbnd(@(x) Rwv(mu0,x,i,parm),0,1,optims);
        Karr(ii) = -Rwv(mu0,nmin,i,parm);
        ii = ii + 1;
    end;
    out = Karr;

end

