
param = zeros(1,7);

param(1) = 0.25; %eta
param(2) = 1; %t_b
param(3) = 2e-7; %p_d
param(4) = 0.99; %V
param(5) = 1.05; %f_EC

param(6) = 0.7; %etas
param(7) = 0.5; %gammas

tdb = 0:0.1:55;
t = 10.^(-tdb./10);

if not (exist('stdplot.mat','file'))
    addpath('../decoy');
    Kstd = stdplot(t,param);
    Kstd = [Kstd; keyrate_wv(t,param)];
    save('stdplot.mat','Kstd');
else
    load('stdplot.mat','Kstd');
end;

if not (exist('stdplot_nv.mat','file'))
    Knv = keyrate_nv(t,param);
    save('stdplot_nv.mat','Knv');
else
    load('stdplot_nv.mat','Knv');
end;

% change optimization parameters
global optims

optims = optimset('fminsearch');
optims.TolFun = 1e-7;
optims.TolX = 1e-7;