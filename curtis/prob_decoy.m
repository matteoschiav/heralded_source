clear all
close all

etas = 0.7;
gammas = 0.5;

mu = 0.2;
m = [2,4,8,16,32,64,128,256,512,1024];

k = floor(log(m)./log(2));

Pda = exp(-etas.*mu.*((2-gammas).*gammas.^(1-m) - 1)./(1-gammas));
Pds = exp(-etas.*mu.*(2.^k)./(gammas.^k));

P = [vec2mat(Pds,1), vec2mat(Pda,1)];

leg = cell(1,2);
leg{1} = 'Symmetric';
leg{2} = 'Asymmetric';
bar(k,P,'grouped');
legend(leg);