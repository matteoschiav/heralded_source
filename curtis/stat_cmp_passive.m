% compare the two photon statistics of the source conditioned to the click
% of at least one detector

close all
clear all

init

set (0,'DefaultAxesFontSize',20);


etas = 0.7;
gammas = 0.5;

mu = 0.25;

m = 32;

n = 0:5;

% asymmetric

Kc = vec2mat(pa_signal(n,mu,m,etas,gammas),1);
Knc = vec2mat(p_decoy(n,mu,etas),1);

K = [Kc,Knc];
figure();
bar(n,K,'grouped');
title('SMHPS');
xlabel('Photon number');
ylabel('Probability');
legend('P^{c}','P^{nc}');

% symmetric

Kcs = vec2mat(ps_signal(n,mu,m,etas,gammas),1);
Kncs = vec2mat(p_decoy(n,mu,etas),1);

figure();
K = [Kc,Knc];
bar(n,K,'grouped');
title('AMHPS');
legend('P^{c}','P^{nc}');
xlabel('Photon number');
ylabel('Probability');

