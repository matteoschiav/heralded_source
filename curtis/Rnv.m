function out = Rnv( mu,nu,t,param)
%RWV normalized rate in the case poissonian source and using Weak+Vacuum
% decoy states (see Curtis et al.)

    eta = param(1);
    t_b = param(2);
    p_d = param(3);
    V = param(4);
    f_EC = param(5);
    
    e0 = 0.5;
    ed = (1-V)./2;
    Y0 = 2.*p_d;
    etac = eta.*t_b.*t;
    
    function o = h(x)
        if (x == 0)
            o = 0;
        else
            o = (-x.*log(x)-(1-x).*log(1-x))./log(2);
        end;
    end;

%     h = @(x) (-x.*log(x)-(1-x).*log(1-x))./log(2);

    Qs = (1 - exp(-mu.*etac) + Y0.*exp(-mu.*etac));
    Es = ( e0.*Y0 + ed.*(1 - exp(-mu.*etac)) ) ./ Qs;
    
    Qd = (1 - exp(-nu.*etac) + Y0.*exp(-nu.*etac));
    Ed = ( e0.*Y0 + ed.*(1 - exp(-nu.*etac)) ) ./ Qd;
        
    p0s = poisspdf(0,mu);
    p1s = poisspdf(1,mu);
    p2s = poisspdf(2,mu);
    p0d = poisspdf(0,nu);
    p1d = poisspdf(1,nu);
    p2d = poisspdf(2,nu);
    
    Y1 = etac + Y0.*(1-etac);
    e1 = (e0.*Y0 + ed.*etac)./(etac + Y0.*(1-etac));
    
%     NO VACUUM
    Y0U = min( [(Qs.*Es./(p0s.*e0)), (Qd.*Ed./(p0d.*e0))] );
    Y0L = max( [((p1s.*Qd - p1d.*Qs)./(p1s.*p0d - p1d.*p0s)), 0] );
    Y1L = max( [((p2s.*Qd - p2d.*Qs - Y0U.*(p2s.*p0d - p2d.*p0s))./(p2s.*p1d-p2d.*p1s)),0] );
    e1U = min( [((Qs.*Es - p0s.*Y0L.*e0)./(p1s.*Y1L)), ((p0d.*Qs.*Es - p0s.*Qd.*Ed)./((p0d.*p1s - p0s.*p1d).*Y1L))] );
    e1U = min( [((Qd.*Ed - p0d.*Y0L.*e0)./(p1d.*Y1L)),e1U] );
    
    Y1Lm = mu./(mu.*nu - nu.^2).*(Qd.*exp(nu) - Qs.*exp(mu).*(nu.^2)./(mu.^2) - (mu.^2 - nu.^2)./(mu.^2).*Y0U);
    e1Um = (Ed.*Qd.*exp(nu) - e0.*Y0L)./(Y1L.*nu);

% VACUUM + WEAK
%     Y1L = mu./(mu.*nu - nu.^2).*(Qd.*exp(nu) - Qs.*exp(mu).*(nu.^2)./(mu.^2) - (mu.^2 - nu.^2)./(mu.^2).*Y0);
%     e1U = (Ed.*Qd.*exp(nu) - e0.*Y0)./(Y1L.*nu);
    
    if (e1U < 0) || (e1U > 0.5) || (Es < 0) || (Es > 0.5)
        out = 0;
    elseif (Y1L < 0) || (Y1L > 1)
        out = 0;
    else
        out = - (p0s.*Y0L + p1s.*Y1L.*(1 - h(e1U)) - Qs.*f_EC.*h(Es));
    end;
end

