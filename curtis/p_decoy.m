function out = p_decoy( n,mu,etas )
%P_DECOY statistics of the decoy state, for both the symmetric
%   and the antisymmetric case

    out = (mu.*(1-etas)).^n ./ factorial(n) .* exp(-mu.*(1-etas));


end

