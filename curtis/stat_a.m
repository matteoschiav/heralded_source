init

etas = param(6);
gammas = param(7);
mu = 0.008183;
m = 32;
k = floor( log(m) ./ log(2) );

n = 0:10;

i = 1:m;
ki = i-1;
ki(m) = m-1;
    
Ai = exp(-mu.*etas.*(gammas.^(1-i)-1)./(1-gammas)) ./ ( 1 - exp(-mu.*etas.*((2-gammas).*gammas.^(1-m)-1)./(1-gammas)) );
bi = exp(-etas.*mu.*(1./(gammas.^ki)-1));
mean = mu.*(1 + etas.*dot(Ai,bi))
Kp = poisspdf(n,mean);
Ka = pa_signal(n,mu,m,etas,gammas);

Kp = vec2mat(Kp,1);
Ka = vec2mat(Ka,1);
K = [Kp, Ka];
bar(n,K,'grouped');

legend('poissonian','heralded asymmetric');