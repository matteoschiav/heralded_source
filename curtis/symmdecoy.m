%clear all;
%close all;

init

set (0,'DefaultAxesFontSize',20);

colors = get(0,'DefaultAxesColorOrder');
colors = [colors; [0.5 0.5 0.5]; [1 1 0]; [0 0.75 0]];
set (0,'DefaultAxesColorOrder',colors);
% m = [2,4,8,16,32,64,128,256,512];
% m = [2,4,8,64];
% m = [2,4,8,16,32,64,256,512];
% m = [32,32,32,32,32,32,32];
m = [2,4,8,32];

% NEW FUNCTION
% FOR etas = 0.7 gammas = 0.5
% m = 2, mf = 0.3;
% m = 4, mf = 0.25;
% m = 8, mf = 0.3;
% m = 16, mf = 0.45;
% m = 32, mf = 0.5;
% m = 64, mf = 0.5;
% m = 256, mf = 0.5;
% m = 512, mf 0.5;

% mf = [0.3,0.25,0.3,0.45,0.5,0.5,0.5,0.5];
mf = [0.3,0.25,0.3,0.5,0.5];


% mf = [0.1,0.2,0.3,0.4,0.5,0.75,1.];
% mf = [0.3,0.35,0.4,0.45,0.5,0.55,0.6];

if not(exist('symmdecoy.mat','file'))
    K = keyrate_s(t,m,param,mf);
    save('symmdecoy.mat','K');
else
    load('symmdecoy.mat','K');
end
% K = []; 

Ktot = [K; Kstd; Knv];

leg = cell(1,length(m)+4);
ii = 1;
for i = m
%     leg{ii} = int2str(i);
    leg{ii} = num2str(i);
    ii = ii + 1;
end;
leg{ii} = 'single photon';
leg{ii+1} = 'WCS';
leg{ii+2} = 'WCS + infinite decoy';
leg{ii+3} = 'WCS + one decoy';



figure('position',[0 0 700 700]);
lk1 = length(Ktot(:,1));
semilogy(tdb,Ktot(1:lk1-4,:),'-','LineWidth',3);
hold on
semilogy(tdb,Ktot(lk1-3,:),':','color','black','LineWidth',2);
cols = lines;
for i = 1:3
    semilogy(tdb,Ktot(lk1-3+i,:),'--','LineWidth',2,'color',cols(i,:));
end
legend(leg,'fontsize',14);
title('SMHPS with passive decoy');
xlabel('Losses L [dB]');
ylabel('Key rate R');
ylim([2e-8 .5]);
xlim([0 55]);


% print -depsc symmdecoy.eps