function out = ps_signal( n,mu,m,etas,gammas )
%PS_SIGNAL symmetric statistics, for the "signal"

    k = floor (log(m)./log(2));
    out = mu.^n .* exp(-mu) ./ factorial(n) .* ( 1 - (1-etas).^n .* exp(-etas.*mu.*(1./(gammas.^k)-1)) ) ./ (1 - exp(-etas.*mu./(gammas.^k)));


end

