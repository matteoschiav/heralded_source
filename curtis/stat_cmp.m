clear all
close all

init

etas = param(6);
gammas = param(7);
% mus = 0.003778;
% mua = 0.008183;
mus = 0.3;
mua = 0.2;
m = 32;
k = floor( log(m) ./ log(2) );

n = 0:10;

Ks = vec2mat(ps_signal(n,mus,m,etas,gammas),1);
Ka = vec2mat(pa_signal(n,mua,m,etas,gammas),1);
K = [Ks,Ka]
bar(n,K,'grouped');
