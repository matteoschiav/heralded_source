La notazione usata nei file della cartella “decoy” si rifà alla review di Scarani. Usa una notazione completamente diversa da quella usata negli articoli sul decoy state (che è poi quella che uso io nel mio testo). Nella cartella “curtis”, invece, si usa la notazione degli articoli sul decoy state.

Le principali discrepanze si hanno in:
- R = Q -> Scarani indica il gain con R mentre negli articoli sul decoy è indicato come Q, negli script di questa cartella uso R, in quelli dell’altra Q
- Q = E -> Scarani usa Q per l’error rate atteso, così come negli script di questa cartella. Gli articoli sul decoy (e il mio testo) indicano questa quantità come E.
 (NOTA) RQ (Scarani) := QE (decoy & mio testo)


Il secret key rate è indicato con K.

DECOY
In questa cartella sono presenti i file per calcolare i rate usando la MHPS, la SMHPS e la AMHPS, sia senza decoy state che con active decoy:
  Kasymm, optimasymm, asymm => AMHPS senza decoy
  KasymmDecoy, optimasymmdecoy, asymm_active => AMHPS con decoy
  Ksymm, optimsymm, symm => SMHPS senza decoy
  KsymmDecoy, optimsymmdecoy, symm_active => SMHPS con decoy
  Ksimple, optimasimple, simple => MHPS senza decoy
  KsimpleDecoy, optimsimpledecoy, simpledecoy => MHPS con decoy
I file K* contengono la funzione che calcola il secret key rate in funzione del mu (dati i parametri della sorgente, vedi init.m), mentre i file optim* cercano il massimo del secret key rate usando fminsearch(), con mu come variabile. L’ultimo file di ogni sequenza si limita a chiamare optim* per sorgenti con un diverso numero di cristalli m.

I file ps e pa restituiscono la statistica di, rispettivamente, SMHPS e AMHPS, dato mu. Il file stat_cmp confronta le due statistiche.
Il file cmp_mu confronta i rate per la SMHPS e l’AMHPS senza decoy state a 9 dB, variando il mu.
I file symm_gamma e asymm_gamma confrontano come variano i secret key rate variando il gamma, rispettivamente, della SMHPS e AMHPS.

La notazione dei file K* rispecchia quella di Scarani (pag.1327-1328).

CURTIS
In questa cartella viene simulato il caso del passive decoy state. L’articolo di riferimento è il Curty, et al. (DOI 10.1103/PhysRevA.81.022310), che è anche la base della notazione usata in questi script.
Nel testo spiego le simulazioni effettuate (le formule sono le stesse e la notazione anche). In particolare Ra e Rs sono i rate per la AMHPS e la SMHPS, usando passive decoy state senza vuoto (solo le due statistiche “Fotone rivelato”, cioè scatta almeno uno degli SPAD della xMHPS, e “Fotone non rivelato”). I keyrate sono calcolati in keyrate*, usando il mu definito in symmdecoy e asymmdecoy. Questo mu è calcolato “a occhio” (provando differenti mu e vedendo quale massimizza il key rate).
Il file Rwv presenta invece il key rate del passive decoy state usando una weak coherent source Weak+Vacuum deocy state. E’ usato per fissare il riferimento di decoy state nel file stdplot.

Il file all_cmp confronta l’AMHPS e la SMHPS quando usate in modo attivo e passivo.

I file ps_signal e pa_signal danno la statistica in uscita nel caso “Fotone rivelato”, mentre p_decoy la fornisce nel caso “Fotone non rivelato”. Il file prob_decoy indica la probabilità dell’evento “Fotone non rivelato”.

I file stat_* fanno confronti tra le varie statistiche.